<?php
//	$data = file_get_contents('fly.json');
//$word="abandonere";

scrapShit("NN");
scrapShit("NB");


function scrapShit($languageToScrap) {
	$files = scandir("./$languageToScrap/");
	$finalInflectionArray = array();
	$finalWordDefinitionArray = array();
	
	foreach($files as $file) {
		//do your work here
		if ($file != "." || $file != "..") {
			$word = substr($file, 0, -5 );
			getDefinitions($word, $finalInflectionArray, $finalWordDefinitionArray);
		}
	}

	// Tests
	$arrayTempNumber = count($finalWordDefinitionArray); // count() can't be the for rule if we're gonna unset from within
	for ($i = 0; $i < $arrayTempNumber; $i++) {
		$testExplode = explode("	", $finalWordDefinitionArray[$i]);
		if (count($testExplode) == 1){
			echo "ERROR - WORD HAS NO DEFINITION BUT IS IN .txt FILE: ".$finalWordDefinitionArray[$i]."\n";
			unset($finalWordDefinitionArray[$i]);
		}
	}

	$arrayTempNumber = count($finalInflectionArray); // count() can't be the for rule if we're gonna unset from within
	for ($i = 0; $i < $arrayTempNumber; $i++) {
		$testExplode = explode(", ", $finalInflectionArray[$i]);
		if (count($testExplode) == 1){
			echo "ERROR - WORD HAS NO INFLECTIONS BUT IS IN .inf FILE: ".$finalInflectionArray[$i]."\n";
			unset($finalInflectionArray[$i]);
		}
	}
	// End of tests

	// Write out arrays to appropriate files
	if ($languageToScrap == "NB"){
		$my_file = '../nb-NOtoENdictionary_Wiktionary.inf';
		$handle = fopen($my_file, 'w') or die('Cannot open file: '.$my_file);
		fwrite($handle, implode("\n", $finalInflectionArray));

		$my_file = '../nb-NOtoENdictionary_Wiktionary.txt';
		$handle = fopen($my_file, 'w') or die('Cannot open file: '.$my_file);
		fwrite($handle, implode("\n", $finalWordDefinitionArray));
	}
	if ($languageToScrap == "NN"){
		$my_file = '../nn-NOtoENdictionary_Wiktionary.inf';
		$handle = fopen($my_file, 'w') or die('Cannot open file: '.$my_file);
		fwrite($handle, implode("\n", $finalInflectionArray));

		$my_file = '../nn-NOtoENdictionary_Wiktionary.txt';
		$handle = fopen($my_file, 'w') or die('Cannot open file: '.$my_file);
		fwrite($handle, implode("\n", $finalWordDefinitionArray));
	}

	//var_dump($finalInflectionArray);
}

function getDefinitions($word, &$finalInflectionArray, &$finalWordDefinitionArray) {
	$data = file_get_contents('./NB/'.$word.'.json');
	$inflectionString = $word;
	$wordDefinitionString = $word;
	$inflectionCheckArray = array($word);
	$json = json_decode($data);
	for ($i = 0; $i < count($json); $i++) { // There can be multiple $json's inside each other each with a number of definitions[]
		for ($z = 0; $z < count($json[$i]->definitions); $z++) {
			$defString = $json[$i]->definitions[$z]->text;
			echo "Raw entry:\n";
			echo $defString."";
			echo "-----------------------------------------------------------------------------------------\n";

			// Explode by new lines, we only need the first line, rest is definitions, not inflections.
			// > "fly n (definite singular flyet, indefinite plural fly, definite plural flya or flyene)"
			$fuck = explode("\n", $defString);

			echo "\nWord definition:\n";
			for ($x = 1; $x < count($fuck); $x++) {
				if($fuck[$x] != ""){
					echo $fuck[$x]."\n";
					$wordDefinitionString = $wordDefinitionString."	".$fuck[$x];
				}
			}

			// explode by ( so first element is "fly n " and second is "definite singular flyet, indefinite plural fly, definite plural flya or flyene)"
			$me = explode("(", $fuck[0]);
			// Trim the ending ) so we end up with "imperative fly, present tense flyr, simple past fløy, past participle flydd or fløyet"
			if (count($me) < 2) {
				fwrite(STDERR, "WARNING: NO INFLECTIONS FOUND FOR THE WORD ".$word.", JUMPING OUT\n");
				continue;
			}
			$kurva = rtrim($me[1],")");
			// Explode by , so we get entries like "past participle flydd or fløyet" or "simple past fløy"
			$inflectionArray = explode(",", $kurva);
			for ($j = 0; $j < count($inflectionArray); $j++) {
				// Some entries are like "past participle flydd or fløyet", so separate by " or "
				$checkForOR = explode(" or ",$inflectionArray[$j]);
				for ($k = 0; $k < count($checkForOR); $k++) {
					$lastExplode = explode(" ", $checkForOR[$k]);
					// Last word in the entry is the inflection
					$currentInflection = end($lastExplode);
					// Following is a check if Inflection is unique, as the inflections can be the same as the original word or repeat
					$shouldInflectionBeAdded = "yes";
					for ($y = 0; $y < count($inflectionCheckArray);$y++) {
						if ($inflectionCheckArray[$y] == $currentInflection || $currentInflection == "" || $currentInflection == "indeclinable" || $currentInflection == "uncountable" || $currentInflection == "gender") {
							$shouldInflectionBeAdded = "no";
						}
					}
					if ($shouldInflectionBeAdded == "yes"){
						array_push($inflectionCheckArray, $currentInflection);
						$inflectionString = $inflectionString.", ".$currentInflection;
					}
				}
			}
		}

//			var_dump($inflectionArray);
			echo "\n----\n\n";
	}
	if ($inflectionString != ""){ // Ehh why is this shit happening? First two lines are blank
		echo "Inflections: $inflectionString\n\n";
		array_push($finalInflectionArray, $inflectionString);
	}
	if ($wordDefinitionString != ""){ // Ehh why is this shit happening? First two lines are blank
		array_push($finalWordDefinitionArray, $wordDefinitionString);
	}
}
?>
